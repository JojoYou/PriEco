Fixes:<br>
	Results from Bing<br>
	<br>
New:<br>
	Shopping tab<br>
	Added changelog<br>
	<br>
Speed:<br>
	image proxy with compression of images. Used for news<br>
	<br>
Security:<br>
	Added Onion version<br>
	Added HSTS<br>
	Added DNSSEC<br>
	<br>
Design:<br>
	Redesigned landing page<br>
	Darker dark results background<br>
	Different font style<br>
	YouTube, news and products:<br>
		Decreased size in web results.<br>
		Added redirect for more button<br>
	Summary:<br>
		changed icon<br>
		replaced animation<br>
		hide when "widgets" are off<br>
		made icon 0.5 opacity<br>
		added more data:<br>
			Images<br>
			IP<br>
			loading time<br>
			SSL<br>
	Wikipedia's signature brightness filter for dark mode<br>
	Footer version updated<br>
	<br>
Crawler:<br>
	fixed issues
