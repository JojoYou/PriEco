<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Privacy Policy | PriEco</title>
    <?php
        $beforePathStyle = "../../";
        include '../../Model/style.php';
    ?>
    <link rel="stylesheet" href="../../css/web.css">
</head>

<body>
    <div class="ppTop">
        <div class="absolute width32 ml-20 mt-20 left0 top0 flex alignC flexDRow">
            <img src="../../View/img/PriEco.webp" alt="logo" class="width100P height40">
            <p class="txt32 ml-10">PriEco</p>
        </div>

        <p>Privacy Policy</p>
        <h1 class="txt55">We give you full control</h1>
        <p>and not tracking!</p>
    </div>
    <div class="flex justConC padding20">
        <div class="privacypolicy">
            <p class="txt16">Last updated: July 03, 2024</p>

            <p><b>We give you full control over which data you share and are transparent in the use of Your data.<br>
                    By default, we do not collect anything!</b></p>

            <p>This Privacy Policy describes Our policies and procedures on the collection, use and disclosure of Your
                information when You use the Service and tells You about Your privacy rights and how the law protects
                You.
            </p>
            <p>We always ask you for permission to collect data, and we always let you know which data are collected and how we use them.</p>
            <br>
            <p><i><b>Disclaimer</b>: Because of increasing bot traffic and even DDoS attacks, we were forced to use third-party protection mechanisms:
                <br>1. Cloudflare: Privacy policy can be found <a href="https://www.cloudflare.com/privacypolicy/" target="_blank" class="link">here</a>.
                <br>2. CrowdSec: Privacy policy can be found <a href="https://www.crowdsec.net/privacy-policy" target="_blank" class="link">here</a>.
                <br><br>We are using third-party ad providers, they have no access to you until you click on their links as any other search result.</i></p>
            <br><br>
            <h2>Interpretation and Definitions</h2>
            <h3>Interpretation</h3>
            <p>The words of which the initial letter is capitalized have meanings defined under the following
                conditions.
                The following definitions shall have the same meaning regardless of whether they appear in singular or
                in
                plural.</p>
            <h3>Definitions</h3>
            <p>For the purposes of this Privacy Policy:</p>
            <p><b>Company</b> (referred to as either &quot;the Company&quot;, &quot;We&quot;,
                &quot;Us&quot; or &quot;Our&quot; in this Agreement) refers to PriEco.</p>

            <p><b>Cookies</b> are small files that are placed on Your computer, mobile device or any other
                device by a website.</p>

            <p><b>Data</b> is any information that is being collected because of actions of an individual.</p>

            <p><b>Service</b> refers to the Website.</p>

            <p><b>Index providers</b> refers to third party search results providers such as Google, Bing, Brave and Mojeek.</p>

            <p><b>You</b> means the individual accessing or using the Service, or the company, or
                other
                legal entity on behalf of which such individual is accessing or using the Service, as
                applicable.
            </p>

            <br><br>
            <h2>Collecting and Using Your Data</h2>
            <h3>Types of Data Collected</h3>
            <h4>Personal Data</h4>
            <p>While using Our Service, We may ask You to provide Us with certain information that can be used to
                contact You or help us improve our Services. This information may include, but is not limited to:
                <br><br>
                <b>1. Email address:</b> Providing us with Your Email address is always voluntary and is used for contacting you back.<br><br>
                <b id="telemetry">2. Telemetry:</b> Telemetry helps us improve our Service and provide fast and reliable experiences for everyone. Telemetry includes:<br>
                • Caching results: When you use external index providers in PriEco, we cache their response for decrease in loading time and eliminating the need to call them again. Caching collects: <b>hashed query</b>, preferred <b>encrypted location</b>, preferred <b>encrypted  language</b> and <b>encrypted results</b><br><br>
                <b>3. Daily search count:</b> We count the number of times users, including you, searched using PriEco. This data includes purely search count and can't be used to identify you.<br><br>
                <b>4. IP address:</b> We never store nor log Your IP address, but we use our own <i>IP to location</i> API to automatically set for you, preferred location and language.
            </p>
            <br>
            <p><b>Cookies or Browser Cookies.</b> Cookies are small pieces of text sent to Your browser by a website you visit. They help that website to remember information about Your visit, which can both make it easier to visit the site again and make the site more useful to you.

            <p>Cookies can be &quot;Persistent&quot; or &quot;Session&quot; Cookies. Persistent Cookies remain on Your
                personal computer or mobile device when You go offline, while Session Cookies are deleted as soon as You
                close Your web browser.
            <p>We use both Session and Persistent Cookies for the purposes set out below:</p>


            <p><b>Necessary / Essential Cookies</b></p>
            <p>Type: Session Cookies<br>
            Administered by: Us<br>
            Purpose: These Cookies provide you with Your own local cache of search results. It is used to drastically improve loading time when you revisit the same results page again in the same session.</p>

            <p>Type: Persistent Cookies<br>
            Administered by: Us<br>
            Purpose: These Cookies are created at Your 1st visit of PriEco and are recreated in case of deletion. Their life span is set to 7 days unless You modify them using our settings. In this case, their life span is set to 1 year.
            <br><br>
            They are: <b>Location</b>, <b>Language</b> and <b>Telemetry</b> (on/off) cookies.</p>
            <br>
            <p><b>Functionality Cookies</b></p>
            <p>Type: Persistent Cookies<br>
            Administered by: Us<br>
            Purpose: These Cookies allow You to modify and save Your preferences for PriEco, such as but not limited to: Theme, Units, Suggestions provider, use of JavaScript and so on.</p>

            <br>

            <h3>Sharing of your data</h3>
            <p>If you use any of the external index providers in PriEco, it is necessary for us to send them your query, preferred location and preferred language so that they can provide us with results for you.</p>

            <br>

            <h3>Transfer of Your Personal Data</h3>
            <p>Your information is processed in the Company's servers and in any
                other
                places where the parties involved in the processing are located. It means that this information may be
                transferred to — and maintained on — computers located outside Your state, province, country or other
                governmental jurisdiction where the data protection laws may differ than those from Your jurisdiction.
            </p>
            <p>Your consent to this Privacy Policy followed by Your submission of such information represents Your
                agreement
                to that transfer.</p>
            <p>The Company will take all steps reasonably necessary to ensure that Your data is treated securely and in
                accordance with this Privacy Policy.</p>

            <br><br>

            <h2>Children's Privacy</h2>
            <p>Our Service does not address anyone under the age of 13. We do not knowingly collect any information from anyone under the age of 13. If You are a parent or guardian, please review our privacy policy and Service before allowing your child to use our Service.</p>

            <br><br>

            <h2>Changes to this Privacy Policy</h2>
            <p>We may update Our Privacy Policy from time to time. We will notify You of any changes by posting the new
                Privacy Policy on this page and notifying You in our Service.</p>
            <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy
                Policy
                are effective when they are posted on this page.</p>

            <br><br>

            <h2>Contact Us</h2>
            <p>If you have any questions about this Privacy Policy, You can contact us:</p>


            <p>By email: support@jojoyou.org</p>


            <p>By visiting this page on our website: <a href="https://prieco.net/feedback.php" rel="external nofollow noopener" target="_blank" class="link">
                https://prieco.net/feedback.php</a> (and providing Us with Your email so that We can contact You back)</p>
        </div>
    </div>

    <?php
        $beforePathFooter = '../../';
        include '../../Model/footer.php';
    ?>

</body>

</html>
