<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Chrome | PriEco</title>
  <link rel="stylesheet" href="../../../css/web.css">
  <?php
  $beforePathStyle = '../../../';
  include('../../../Model/style.php');
  ?>
</head>

<body>
  <div class="headerChrome headerDefault"></div>
  <h1 class="width100P mt-15 flex wrap centerTxt flexDRow justConC alignC">Set PriEco in your Chromium<img class="width50"
      src="../../../View/img/web/tutorials/default/chrome/chromes.webp">browser</h1>

  <div class="width100P flex justConC">
    <a href="./">
      <button class="borderRadius borderNone bgTop padding10 colorWhite Pointer mt-10"><b>Not a Chromium user?</b></button>
    </a>
  </div>

  <div class="width100P flex alignC flexDColumn mt-150">
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden">
<img class="width100P"
          src="../../../View/img/web/tutorials/default/chrome/1.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Open your Chromium browser</h3>
    </div>

    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/chrome/2.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Open settings and navigate to search engines</h3>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/chrome/3.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Click on manage search engines</h3>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/chrome/4.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Click on Add</h3>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/chrome/5.webp">

      <div class="paddingT20 paddingB20 paddingL20 paddingR20">
        <h3>Fill up details</h3>
        <p>Search engine: PriEco<br>Shortcut: :p<br>URL: https://search.jojoyou.org/?q=%s</p>
      </div>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/chrome/6.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Click on 3 dots and click on make default</h3>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
    <img class="width100P"
        src="../../../View/img/web/tutorials/default/chrome/7.webp">

    <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">You are done 🎉</h3>
  </div>
  </div>
  <div class="width100P flex justConC mt-150">
    <a href="https://search.jojoyou.org/">
      <button class="borderRadius borderNone bgTop padding10 colorWhite Pointer mt-10"><b>Visit PriEco</b></button>
    </a>
  </div>
  <?php include('../../../Model/footer.php'); ?>
</body>

</html>