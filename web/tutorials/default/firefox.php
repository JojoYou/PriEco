<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Firefox | PriEco</title>
  <link rel="stylesheet" href="../../../css/web.css">
  <?php
  $beforePathStyle = '../../../';
  include('../../../Model/style.php');
  ?>
</head>

<body>
  <div class="headerFirefox headerDefault"></div>
  <h1 class="width100P mt-15 flex wrap centerTxt flexDRow justConC alignC">Set PriEco in your Firefox<img class="width50"
      src="../../../View/img/web/tutorials/default/firefox/firefox.webp">browser</h1>

  <div class="width100P flex justConC">
    <a href="./">
      <button class="borderRadius borderNone bgTop padding10 colorWhite Pointer mt-10"><b>Not a Firefox user?
        </b></button>
    </a>
  </div>

  <div class="width100P flex alignC flexDColumn mt-150">
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/firefox/1.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Open your Firefox browser</h3>
    </div>

    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/firefox/2.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Type search.jojoyou.org into URL bar and press enter</h3>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/firefox/3.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Right click on URL bar and click on Add "PriEco"</h3>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/firefox/4.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Go to Settings</h3>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/firefox/5.webp">

      <div class="paddingT20 paddingB20 paddingL20 paddingR20">
        <h3>Click on Search</h3>
      </div>
    </div>
    <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
      <img class="width100P"
          src="../../../View/img/web/tutorials/default/firefox/6.webp">

      <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">Select PriEco</h3>
    </div>
  <div class="max-width700 widthAuto ml-10 mr-10 centerTxt whiteAblackBg borderRadius overflowHidden mt-150">
    <img class="width100P"
        src="../../../View/img/web/tutorials/default/firefox/7.webp">

    <h3 class="paddingT20 paddingB20 paddingL20 paddingR20">You are done 🎉</h3>
  </div>
  </div>
  <div class="width100P flex justConC mt-150">
    <a href="https://search.jojoyou.org/">
      <button class="borderRadius borderNone bgTop padding10 colorWhite Pointer mt-10"><b>Visit PriEco</b></button>
    </a>
  </div>
  <?php include('../../../Model/footer.php'); ?>
</body>

</html>